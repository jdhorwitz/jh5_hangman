package com.example.jhorwitz;

public interface HangmanUpdate {
	public void updateMessage(String s);
	public void gameIsDone(boolean winner);
}
