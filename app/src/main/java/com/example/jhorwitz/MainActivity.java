package com.example.jhorwitz;

//MainActivity.java
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.example.jh3_chasselb.R;

import java.util.ArrayList;

public class MainActivity extends Activity
    implements OnClickListener, HangmanUpdate {


    //private String[] words={"aardvark", "elephant", "tiger", "porcupine", "zebra", "monkey","dog", "cat",
     //       "horse", "cow", "pig", "snake",  "hippopotamus",
      //         "alligator", "gorilla", "cheetah", "crocodile", "baboon", "donkey" };

    static  int MAX_GUESSES ;
    boolean gameIsOver = false;

    TextView display=null;

    MyDrawHangmanView myDrawView;
    HangmanLogic hangmanLogic;
    Button callWordManager;

    int[] buttonResources = {
            R.id.a, R.id.b, R.id.c, R.id.d, R.id.e, R.id.f, R.id.g,
            R.id.h, R.id.i, R.id.j, R.id.k, R.id.l, R.id.m, R.id.n,

            R.id.o, R.id.p, R.id.q, R.id.r, R.id.s, R.id.t, R.id.u,
            R.id.v, R.id.w, R.id.x, R.id.y, R.id.z};
    Button[] buttons = new Button[buttonResources.length];

    @Override
    public void onCreate(Bundle savedInstanceState) {
        callWordManager = (Button)findViewById(R.id.callWordManager);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);  // Set up click listeners for all the buttons
        for (int i=0; i < buttonResources.length; i++)
        {
            Button b = (Button)findViewById(buttonResources[i]);
            b.setOnClickListener(this);
            buttons[i] = b;
        }
        display = (TextView) findViewById(R.id.display);
        myDrawView = (MyDrawHangmanView) findViewById(R.id.myDrawHangmanView);
        MAX_GUESSES = MyDrawHangmanView.MAX_GUESSES;
        hangmanLogic = new HangmanLogic(this);

        callWordManager.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                intent.putExtra("words", words);
                //intent.putExtra("category", category);

                startActivityForResult(intent, 1 /* Request */);

            }
        });




    }
    public void onActivityResult(int reqCode, int resCode, Intent data) {
        super.onActivityResult(reqCode, resCode, data);

        String lineSeparator = System.getProperty("line.separator"); // ""/n" on Linux
        if (resCode != Activity.RESULT_OK)
        {
            Log.d("Mine", "Returning without proper Result code");
            return;
        }
        switch(reqCode)
        {
            case 2:
                ArrayList<String> words = data.getStringArrayListExtra("words");

                StringBuilder sb = new StringBuilder();
                for (int i=0; i< words.size(); i++)
                    sb.append(words.get(i)+ lineSeparator);
                callWordManager.setText(sb.toString());
                break;
            default:
                Log.d("Mine","Shouldn't happen");
                break;

        }

    }


    public void updateMessage(String s)
    {
        display.setText(s);
    }
    public void gameIsDone(boolean winner)
    {
        gameIsOver = true;
    }

    private void newGame()
    {
        gameIsOver = false;
        myDrawView.reset();

        for (int i=0; i < buttons.length; i++)
        {
            buttons[i].setVisibility(View.VISIBLE);
        }
        Intent intent = new Intent();
        intent.putExtra("wordManager", words);
        setResult(Activity.RESULT_OK, intent);
        hangmanLogic.newGame(MAX_GUESSES, words);
    }


     public void onClick(View v) {
         int index = v.getId();

         if (gameIsOver)
             return;
         for (int i=0; i < 26; i++)
         {
             if (buttonResources[i]== index)
             {
                 if (buttons[i].isShown())
                 {
                     String s = buttons[i].getText().toString();
                     Log.d("Mine", "onClick: ="+s);
                     char c = s.charAt(0);
                     if (!hangmanLogic.buttonClicked(c))
                         myDrawView.anotherMiss();
                     buttons[i].setVisibility(View.INVISIBLE);
                 }
             }
         }


     }
     public boolean onCreateOptionsMenu(Menu menu)
     {
         super.onCreateOptionsMenu(menu);
         MenuInflater inflater = getMenuInflater();
         inflater.inflate(R.menu.mymenu, menu);
         return true;
     }
     public boolean onOptionsItemSelected(MenuItem item)
     {
         switch (item.getItemId())
         {
         case R.id.newWord:
             Log.d("Mine", "New Word");
             newGame();
             return true;
         }
         case R.id.newCategory:
            Log.d("Mine", "New Category");
            

         return false;
     }


}
