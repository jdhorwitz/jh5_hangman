package com.example.jhorwitz;

//MyDrawHangmanView.java
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.example.jh3_chasselb.R;


public class MyDrawHangmanView extends View  {
  
  public static final int MAX_GUESSES = 10;

  private float width;    
  private float height;  
  Paint background, translucentRedPen, bluePen, headPen, platformPen;
  
  int numMisses =0;
  
  public void anotherMiss()
  {
      numMisses+=1;
      invalidate();
  }
  public void reset()
  {
      numMisses = 0;
      invalidate();
  }

   

  private void init()
  {
      Log.d("Mine","init called in MyDrawHangmanView");
      background = new Paint();
      //background.setStyle(Paint.Style.FILL_AND_STROKE);
      background.setColor(0xffcfffff);        

      headPen = new Paint();
      headPen.setColor(0xffff0000);
      platformPen = new Paint();
      platformPen.setColor(0xff00ffff);

      translucentRedPen = new Paint();
      translucentRedPen.setColor(getResources().getColor(R.color.translucentRedPen));

      bluePen = new Paint();
      bluePen.setColor(getResources().getColor(R.color.bluePen));
      bluePen.setStyle(Paint.Style.STROKE);
      bluePen.setStrokeWidth(2);
  }
  public MyDrawHangmanView(Context context) {
      super(context);
      init();
  }
  public MyDrawHangmanView(Context context, AttributeSet attrs) {
      super(context, attrs);
      init();
  }

  public MyDrawHangmanView(Context context, 
          AttributeSet ats, 
          int defaultStyle) {
      super(context, ats, defaultStyle);
      init();
  }
  // ...





  @Override
  protected void onDraw(Canvas canvas) {
      // Draw the background...

      width = getWidth();
      height = getHeight();
      float centerX = width/2;
      float centerY = height/2;
      float radius = Math.min(width, height)/2;

      Log.d("Mine", "onDraw: width="+width + " height="+height);
      if (numMisses == 0)return;
      canvas.drawRect(0, 0, width, height, background);
      
      canvas.save();
      canvas.scale(width/100, height/100);
      
      for (int i=1; i <= numMisses; i++)
      {
          switch(i)
          {
          case 1:            
              canvas.drawRect(5,90,96,96, platformPen); // Base
              break;
          case 2:
              canvas.drawLine(90, 90, 90, 10, bluePen); // Pole
              break;
          case 3:
              canvas.drawLine(91,10, 19,10, bluePen); // Upper bar
              break;
          case 4:
              canvas.drawLine(20,10,20,20, bluePen); // tie
              break;
          case 5:
              canvas.drawCircle(20, 25, 5, headPen); //Head
              break;
          case 6:
              canvas.drawLine(20, 30, 20, 55, bluePen); // Body
              break;
          case 7:
              canvas.drawLine(20, 30, 15, 45, bluePen); // left arm
              break;
          case 8:
              canvas.drawLine(20, 30, 25, 45, bluePen); // right arm
              break;
          case 9:
              canvas.drawLine(20, 55, 15, 70, bluePen); // left leg
              break;
          case MAX_GUESSES:
              canvas.drawLine(20, 55, 25, 70, bluePen); // left leg
          
              break;
          }
      }
      
      canvas.restore();        

      canvas.drawCircle(centerX, centerY, radius,  translucentRedPen);
      
  
      
  }



}
